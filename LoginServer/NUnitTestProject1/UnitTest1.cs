using FirebaseAdmin.Auth;
using LoginServer.Models;
using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NUnitTestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            //DeleteAll_LOGINTEST();
        }


        #region 1:10 통신, Firebase DB 추가
        
        public async Task Test11()
        {
            int random = new Random().Next(1, 100);
            int sr = new Random().Next(100, 10000);
            var r = await SDReport.AddScore($"nick{random}", sr);

            //await SDReport.ReadTest();
            Console.WriteLine();
        }
        #endregion


        #region 모든 DB 삭제

        public async Task DeleteAll_LOGINTEST()
        {
            await SDReport.DeleteAll_LOGINTEST();
        }
        #endregion



        #region Add User
        /// <summary>
        /// ID는 중복 될 수 없다.
        /// </summary>
        /// <returns></returns>
        
        public async Task Test3()
        {
            int random = new Random().Next(1, 100);
            int sr = new Random().Next(100, 10000);
            User user = new User { ID = $"ID_{random}", Password = $"{sr}" };
            var r = await SDReport.AddUser(user);
            
        }
        #endregion

        #region ID 구분
        /// <summary>
        /// DB에 있으면 ture, 없으면 false
        /// </summary>
        /// <returns></returns>
        /// 
        public async Task SamID()
        {
            var isResult = await SDReport.ID_DuplicationCheck("SD_2330", "000");
        }

        #endregion

        #region ID 10개 생성
        public async Task Test5()
        {
            for (int i = 0; i < 10; i++)
            {
                await SDReport.AddUser(new User { ID=$"SD_{i}", Password = $"{i*100}"});
            }
        }

        #endregion


        #region 사용자 인증 
        [Test]
        public async Task GetUserData()
        {
            // 데이터 검색
            //string uid = "test_1";
            //UserRecord userRecord = await FirebaseAuth.DefaultInstance.GetUserAsync(uid);


            // 사용자 만들기


            //var provider = new firebase.auth.GoogleAuthProvider();

            //var firebase = new FirebaseClient(firebaseUrl, new FirebaseOptions { AuthTokenAsyncFactory = () => Task.FromResult(authToken) });

        }




        #endregion

    }
}


