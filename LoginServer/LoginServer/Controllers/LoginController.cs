﻿using LoginServer.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LoginServer.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult LoginView()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LoginView(string email, string password)
        {
            #region 헤쉬 코드를 이용한 비번 암호화
            var hasher = new PasswordHasher<string>();
            var list = await SDReport.GetAllDB();

            string sPassword = null;
            string sID = null;
            foreach (var item in list)
            {
                if (item._user.ID == email)
                {
                    sID =  item._user.ID;
                    sPassword = item._user.Password;
                    break;
                }
            }
            var result = hasher.VerifyHashedPassword(null, sPassword, password);

            if (result.ToString() == "Success")
            {
                var Claims = new List<Claim>()
                {
                    new Claim("id", sID),
                    new Claim("paswword", sPassword)
                };
                var ci = new ClaimsIdentity(Claims, CookieAuthenticationDefaults.AuthenticationScheme);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme
                    , new ClaimsPrincipal(ci)); // 옵션

                return LocalRedirect(Url.Content("~/counter"));
            }

            #endregion



            #region 헤쉬 코드
            //var hasher = new PasswordHasher<string>();
            //var test_password = password;

            #endregion


            #region 로그인 성공 확인
            //var list = await SDReport.GetAllDB();

            //string sPassword = null;
            //foreach (var item in list)
            //{
            //    if(item._user.ID == email)
            //    {
            //        sPassword = item._user.Password;
            //        break;
            //    }
            //}

            //var result = hasher.VerifyHashedPassword(null, sPassword, password);
            #endregion


            #region 회원가입 암호화
            //var hashed = hasher.HashPassword(null, test_password);
            //User user = new User { ID = email, Password = hashed };
            //var isResult = await SDReport.AddUser(user);
            #endregion

            //return LocalRedirect(Url.Content("~/"));
            return View();
        }




        


        [Authorize]
        public IActionResult UserInfor() => View();
    }
}
