﻿using Firebase.Database;
using Firebase.Database.Query;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



/*
 var firebaseConfig = {
    apiKey: "AIzaSyA8dN_UZMXoVplMbVOcCD5gXLIyIAD62Pg",
    authDomain: "logindbsample.firebaseapp.com",
    databaseURL: "https://logindbsample-default-rtdb.firebaseio.com",
    projectId: "logindbsample",
    storageBucket: "logindbsample.appspot.com",
    messagingSenderId: "599777380896",
    appId: "1:599777380896:web:dc2915b513c258c9b8ca45",
    measurementId: "G-2EFXW2R5D0"
  };
 * 
 * */

namespace LoginServer.Models
{
    public class SDReport
    {

        public class timestamp
        {
            [JsonProperty(".sv")]
            public string sv = "timestamp";
        }

        public const string databaseURL = "https://logindbsample-default-rtdb.firebaseio.com/";
        private const string db_password_master = "JvrDgmdBBSc08ZwazPRhcYpF6QoUOFWFGhrmH8tv";
        private const string LEADER_BOARD = "leaderboard";
        private static int LIMIT_COUNT = 30;
        private static TimeSpan default_timeout = TimeSpan.FromSeconds(30);
        static FirebaseClient fb = null;


        private const string LOGINTEST = "logintest";
        static SDReport()
        {
            try
            {
                fb = new FirebaseClient(databaseURL, new FirebaseOptions()
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(db_password_master)
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


        //-------------------------------------------------------- Instance
        #region Instance

        public string nick;
        [JsonIgnore]
        public int score;
        public object time = new timestamp();
        public User _user { get; set; } = new User();


        [JsonIgnore]
        public int rank { get; set; }
        [JsonIgnore]
        public string key { get; set; }

        public override string ToString()
        {
            return $"[{key}][{rank}]nick : {nick} , score : {score} , time : {time}";
        }

        #endregion Instance





        //-------------------------------------------------------- ItemControl
        #region ItemControl

        /// <summary>
        /// <para/>
        /// 1)닉네임과 점수를 추가하면 key값을 반환하는 함수입니다.<br/>
        /// 2)이 함수를 사용할 때에만 Key값이 생성됩니다.<br/> 
        /// 3)닉네임은 중복될 수 있으며, key값으로 구분해야 합니다.<br/>
        /// 4)반환된 키값은 랭킹을 찾기위해 필요하니 변수에 저장하십시오.<br/>
        /// </summary>
        /// <param name="nick">닉네임 매개 변수 타입은 "string"입니다.</param>
        /// <param name="score">점수 매개 변수 타입은 "int"입니다.</param>
        /// <returns></returns>
        public static async Task<string> AddScore(string nick, int score)
        {
            try
            {
                var r = await fb.Child(LEADER_BOARD).PostAsync<SDReport>(
                   new SDReport()
                   {
                       nick = nick,
                       score = score
                   }
                );

                //하지 않음
                //await EliminateItem();

                if (r.Key != null && string.IsNullOrEmpty(r.Key) == false)
                    return r.Key;
                else
                    return null;
            }
            catch (FirebaseException ex)
            {
                Console.WriteLine(ex.ToString());

                return null;
            }

        }

        public static async Task<string> AddUser(User user)
        {
            try
            {
                var r = await fb.Child(LOGINTEST).PostAsync<SDReport>(
                    new SDReport()
                    {
                       _user = user
                    }
                );

                if (r.Key != null && string.IsNullOrEmpty(r.Key) == false)
                    return r.Key;
                else
                    return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }

        #endregion


        public static async Task ReadTest()
        {
            
            var r = await fb.Child("test").OnceAsync<object>();

            Console.WriteLine();
        }


        //-------------------------------------------------------- Database Control
        #region Database Control
        /// <summary>
        /// 1) 모든 DB를 삭제하는 함수이니 사용에 유의 하시기 바랍니다.<br/>
        /// ex) await BCRank.DeleteAllData();
        /// </summary>
        /// <returns></returns>
        public static async Task DeleteAllData()
        {
            await fb.Child(LEADER_BOARD).DeleteAsync();
        }
        public static async Task DeleteAll_LOGINTEST()
        {
            await fb.Child(LOGINTEST).DeleteAsync();
        }

        /// <summary>
        /// 1) 높은 점수 순위 부터 차례로 정렬 해주는 함수입니다. <br/>
        /// 2) DB 사용량에 영향을 주므로 사용에 유의하시기 바랍니다.<br/>
        /// ex) var snap = await Snapshot();
        /// </summary>
        /// <returns></returns>
        public static async Task<SDReport[]> Snapshot()
        {
            var r = await fb.Child(LEADER_BOARD).OnceAsync<SDReport>();
            if (r.Count < 1) return null;

            List<SDReport> list = new List<SDReport>();
            int total = r.Count;

            for (int i = 0; i < r.Count; i++)
            {
                string key = r.ElementAt(i).Key;
                SDReport item = r.ElementAt(i).Object;
                item.key = key;
                list.Add(item);
            }

            list.Sort(delegate (SDReport x, SDReport y)
            {
                return y.score.CompareTo(x.score);
            });

            for (int i = 0; i < list.Count; i++)
            {
                list[i].rank = i + 1;
            }

            /*
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            */
            if (list.Count == 0)
            {
                return null;
            }
            return list.ToArray();
        }


        /// <summary>
        /// 모든 DB가져오기
        /// </summary>
        /// <returns></returns>
        public static async Task<SDReport[]> GetAllDB()
        {
            try
            {
                var r = await fb.Child(LOGINTEST).OnceAsync<SDReport>();
                if (r.Count < 1)
                    return null;


                List<SDReport> list = new List<SDReport>();
                for (int i = 0; i < r.Count; i++)
                {
                    string key = r.ElementAt(i).Key;
                    SDReport sDReport = new SDReport();
                    sDReport._user = r.ElementAt(i).Object._user;
                    sDReport._user.Key = key;
                    sDReport.time = r.ElementAt(i).Object.time;
                    list.Add(sDReport);
                }

                return list.ToArray();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }

            
        }

        #endregion Database Controll




        public static async Task<bool> ID_DuplicationCheck(string id, string password = null)
        {
            try
            {
                var list = await GetAllDB();
                foreach (var item in list)
                {
                    if (item._user.ID == id)
                        return true;
                }
                return false;
            }
            catch (Exception e) 
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }

    }

}
