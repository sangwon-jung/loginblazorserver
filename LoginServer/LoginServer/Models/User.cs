﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginServer.Models
{
    public class UserInfor
    {
        public string Name { get; set; }
        public string  NicName { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
    }



    public class User
    {
        public string ID { get; set; }
        public string Password { get; set; }
        public string Key { get; set; }
    }
}
